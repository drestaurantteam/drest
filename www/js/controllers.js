var Scan = function(scanner){
    scanner.scan(function(rs){
           console.log("Scaned", rs);

           if(rs.text.split(";").length >1)
             window.location = "#/tab/map/"+rs.text;
    })
}


var toRoute = function(coordinates){

          var coordinates = coordinates || '10.424128;-75.549073';

           if(coordinates.split(";").length >1)
             window.location = "#/tab/map/"+coordinates;

}

var map;
var getUserLocation;
var calcRoute;

angular.module('takeme.controllers', [])

.controller('DashCtrl', function($scope) {})
.controller('menuCtrl', function($scope, $rootScope, $stateParams) {

 $scope.title = $stateParams.id || "Menú";

  var menu = {
        mariscos : [
             {name:"Filete de salmón", price: "$40.000", pic: "http://www.ciudadcorazon.net/v2x/images/resized/images/cc/vida_estilo/comida_bebida/salmon_cilantro_200_200.jpg"},
             {name:"Langostinos al ajillo", price: "$45.000", pic: "http://www.hola.com/imagenes/cocina/recetas/2012091760700/brocheta-india-langostinos/0-215-106/brocheta-india-1-b.jpg"},
             {name:"Filete de pescado en salsa de camaron", price: "$56.000", pic: "http://4.bp.blogspot.com/_VBn7F4Opu5o/S9wjF-NG1eI/AAAAAAAAACw/XT9MF-b7hQs/s200/filete-de-pescado.jpg"}
          ],
          entradas : [
             {name:"Ensalada cesar", price: "$15.000", pic: "http://www.guiainfantil.com/pictures/recetas/4555-2-ensalada-cesar-con-pollo-casera.jpg"},
             {name:"Panes al ajo", price: "$35.000", pic: "http://www.abretuboca.com/wp-content/uploads/et_temp/panconoajo01-e1361213202610-24790_200x200.jpeg"},
             {name:"Chips de platano verde", price: "$10.000", pic: "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash2/v/t1.0-1/c6.0.200.200/p200x200/1383097_376269955839958_896667348_n.jpg?oh=1b46e3e3bf67f54c133f59d051a1af68&oe=561B23E7&__gda__=1445932469_7374c1768f1e6b46b736d80056908e25"}
          ]
  }


  $scope.loadItems = function(){
         $scope.items = menu[$stateParams.id] || [];
  }

})
.controller('mainCtrl', function($scope, $rootScope, $storage) {

        $rootScope.settings = $storage.get("settings");

      if(document.getElementById("map"))
      document.getElementById("map").remove();


      var scan = function(){

      console.log("scan");

        Scan(window.cordova.plugins.barcodeScanner);

    }

    $scope.toRoute = toRoute;

    $scope.scan = function(){

         scan();
    }


  getUserLocation = function(callback){

  
var onSuccess = function(position) {

  if (typeof callback === "function")
      callback({latlng:{lat:position.coords.latitude,lng:position.coords.longitude},position:position});
  else
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n');
};


function onError(error) {
    console.log('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

var options = {};

!$rootScope.settings.maxPrecision || (options.enableHighAccuracy =  true);


navigator.geolocation.getCurrentPosition(onSuccess, onError, options);


}


})
.controller('scanCtrl', function($scope) {


    var scan = function(){

      console.log("scan");

        Scan(window.cordova.plugins.barcodeScanner);

    }

    document.addEventListener("deviceReady", scan, false);



})
.controller('mapCtrl', function($scope, $stateParams) {
  
  console.log($stateParams.route);

  var to = $stateParams.route.split(';');
  to = {lat:to[0],lng:to[1]};

  console.log("To", to);
  
  var directionsDisplay;
  var directionsService;

  function initialize() {
  directionsService = new google.maps.DirectionsService();  
  directionsDisplay = new google.maps.DirectionsRenderer();
  var chicago = new google.maps.LatLng(41.850033, -87.6500523);
  var mapOptions = {
    zoom:7,
    center: chicago
  };
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  directionsDisplay.setMap(map);

}

initialize();


  getUserLocation(function(loc){
     
     console.log("Direct", loc);


 calcRoute = function() {
  var start = new google.maps.LatLng(loc.latlng.lat, loc.latlng.lng);
  var end = new google.maps.LatLng(to.lat, to.lng);
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

calcRoute();


  })

})


.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
  $scope.chats = Chats.all();

  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $rootScope, $storage) {
   
     $scope.save = function(){
      console.log("saving settings");

        $storage.save("settings", $rootScope.settings);
     }

     $scope.load = function(){
      console.log("loading settings");
      if(!$rootScope.settings)
        $rootScope.settings = $storage.get("settings");

     }

});
